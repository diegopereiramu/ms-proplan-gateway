import { validarToken } from './middlewares/auth';
const gateway = require('fast-gateway');

const server = gateway({
  routes: [
    {
      middlewares:[],
      prefix: process.env.MS_PATH_AUTH,
      target: process.env.MS_HOST_AUTH,
    },
    {
      middlewares:[],
      pathRegex: '/health',
      prefix: process.env.MS_PATH_SINCRONIZADOR,
      target: process.env.MS_HOST_SINCRONIZADOR,
    },         
    {
      middlewares:[validarToken],
      prefix: process.env.MS_PATH_SINCRONIZADOR,
      target: process.env.MS_HOST_SINCRONIZADOR,
    },    
    {
      middlewares:[],
      pathRegex: '/health',
      prefix: process.env.MS_PATH_PROFESIONALES,
      target: process.env.MS_HOST_PROFESIONALES,
    },        
    {
      middlewares:[validarToken],
      prefix: process.env.MS_PATH_PROFESIONALES,
      target: process.env.MS_HOST_PROFESIONALES,
    },
    {
      middlewares:[],
      pathRegex: '/health',
      prefix: process.env.MS_PATH_CONTRATOS,
      target: process.env.MS_HOST_CONTRATOS,
    },        
    {
      middlewares:[validarToken],
      prefix: process.env.MS_PATH_CONTRATOS,
      target: process.env.MS_HOST_CONTRATOS,
    },     
    {
      middlewares:[],
      pathRegex: '/health',
      prefix: process.env.MS_PATH_PROGRAMACIONES,
      target: process.env.MS_HOST_PROGRAMACIONES,
    },                         
    {
      middlewares:[validarToken],
      prefix: process.env.MS_PATH_PROGRAMACIONES,
      target: process.env.MS_HOST_PROGRAMACIONES,
    },
    {
      middlewares:[],
      pathRegex: '/health',
      prefix: process.env.MS_PATH_USUARIOS,
      target: process.env.MS_HOST_USUARIOS,
    },     
    {
      middlewares:[validarToken],
      prefix: process.env.MS_PATH_USUARIOS,
      target: process.env.MS_HOST_USUARIOS,
    },       
    {
      middlewares:[],
      prefix: process.env.MS_PATH_CATALOGOS,
      target: process.env.MS_HOST_CATALOGOS,
    },        
  ]
});

server.start(process.env.APP_PORT || 2000);