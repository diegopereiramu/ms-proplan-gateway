const jwt = require('njwt');
const fetch = require('node-fetch');

const obtenerToken = (headers) => {
    let authorization = headers['authorization'];
    let token = '';
    if(authorization !== null && authorization !== undefined && authorization !== ''){
        authorization = authorization.split('Bearer ');    
        if(authorization.length == 2){
            token = authorization[1];
        }
    }
    return token;
}

const verificarBlacklist = async (token) => {
    const response = await fetch(process.env.MS_HOST_AUTH+'/token/blacklist',
    {
        method: 'post',
        body:    JSON.stringify({token:token}),
        headers: { 'Content-Type': 'application/json' },
    });
    const data = await response.json();
    return data.OK;
}

const validarToken = async (req, res, next) => {
    let message = 'No Authorizado';
    let code = null;
    let token = obtenerToken(req.headers);
    if(token == ''){
        code = 403;
    }

    if(code == null){
        try{
            const result = jwt.verify(token, process.env.JWT_SECRET_KEY);
            if(!await verificarBlacklist(token)){
                return next();
            }else{
                code = 403;
            }
        }catch(e){
            console.log(e);
            code = 403;
        }
    }

    if(code !== null){
        res.send({
            OK:false, 
            message: message
        }, code);
    }
};

export {
    validarToken,
}